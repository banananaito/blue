﻿using Accord.Imaging.Converters;
using Accord.MachineLearning;
using Accord.Math;
using Accord.Math.Distances;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;
using System.Text;

namespace ImageClustering
{
    public class Cluster
    {
        public int[,] Data { get; set; }

        public Cluster()
        {

        }

        private int getMaximumIntensity()
        {
            int maximumIntensity = 0;

            for (int x = 0; x < Data.GetLength(0); x++)
            {
                for (int y = 0; y < Data.GetLength(1); y++)
                {
                    if (maximumIntensity < Data[x, y])
                        maximumIntensity = Data[x, y];
                }
            }

            return maximumIntensity;
        }

        private int getMinimumIntensity()
        {
            int minimumIntensity = 255;

            for (int x = 0; x < Data.GetLength(0); x++)
            {
                for (int y = 0; y < Data.GetLength(1); y++)
                {
                    if (minimumIntensity > Data[x, y])
                        minimumIntensity = Data[x, y];
                }
            }

            return minimumIntensity;
        }

        private void initializeIntensity(Bitmap image, int width, int height)
        {
            Data = new int[width, height];

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    var pixel = image.GetPixel(x, y);
                    int intensity = pixel.R;
                    if (intensity < pixel.G)
                        intensity = pixel.G;
                    if (intensity < pixel.B)
                        intensity = pixel.B;

                    Data[x, y] = intensity;
                }
            }
        }

        public Bitmap ClusterizeImage(Bitmap image, int width, int height, int clusterCount)
        {
            var resizedImage = resizeImage(image, width, height, InterpolationMode.HighQualityBicubic);
            initializeIntensity(resizedImage, width, height);

            int maximumIntensity = getMaximumIntensity();
            int minimumIntensity = getMinimumIntensity();
            int intensityMultiplier = 255 / (clusterCount - 1);
            int greyThreshold = (maximumIntensity - minimumIntensity) / clusterCount + 1;

            for (int x = 0; x < Data.GetLength(0); x++)
            {
                for (int y = 0; y < Data.GetLength(1); y++)
                {
                    int intensityLevel = (Data[x, y] - minimumIntensity) / greyThreshold;
                    int intensity = intensityLevel * intensityMultiplier;


                    Data[x, y] = intensity;

                    resizedImage.SetPixel(x, y, Color.FromArgb(255, intensity, intensity, intensity));
                }
            }

            return resizeImage(resizedImage, image.Width, image.Height);
        }

        public Bitmap ClusterizeImageKMeans(Bitmap image, int width, int height, int clusterCount)
        {
            var resizedImage = resizeImage(image, width, height, InterpolationMode.HighQualityBicubic);

            // Create converters to convert between Bitmap images and double[] arrays
            var imageToArray = new ImageToArray(min: -1, max: +1);
            var arrayToImage = new ArrayToImage(resizedImage.Width, resizedImage.Height, min: -1, max: +1);

            // Transform the image into an array of pixel values
            double[][] pixels; imageToArray.Convert(resizedImage, out pixels);

            Accord.Math.Random.Generator.Seed = 0;

            // Create a K-Means algorithm using given k and a
            //  square Euclidean distance as distance metric.
            KMeans kmeans = new KMeans(k: clusterCount)
            {
                Distance = new SquareEuclidean(),

                // We will compute the K-Means algorithm until cluster centroids
                // change less than 0.5 between two iterations of the algorithm
                Tolerance = 0.05
            };


            // Learn the clusters from the data
            var clusters = kmeans.Learn(pixels);

            // Use clusters to decide class labels
            int[] labels = clusters.Decide(pixels);

            // Replace every pixel with its corresponding centroid
            double[][] replaced = pixels.Apply((x, i) => clusters.Centroids[labels[i]]);

            // Retrieve the resulting image (shown in a picture box)
            Bitmap result; arrayToImage.Convert(replaced, out result);

            initializeIntensity(result, width, height);

            for (int x = 0; x < Data.GetLength(0); x++)
            {
                for (int y = 0; y < Data.GetLength(1); y++)
                {
                    int intensity = Data[x, y];

                    result.SetPixel(x, y, Color.FromArgb(255, intensity, intensity, intensity));
                }
            }  

            return resizeImage(result, image.Width, image.Height);
        }

        private Bitmap resizeImage(Bitmap image, int width, int height, InterpolationMode interpolationMode = InterpolationMode.NearestNeighbor)
        {
            var destRect = new Rectangle(0, 0, width, height);
            var destImage = new Bitmap(width, height);

            destImage.SetResolution(image.HorizontalResolution, image.VerticalResolution);

            using (var graphics = Graphics.FromImage(destImage))
            {
                graphics.CompositingMode = CompositingMode.SourceCopy;
                graphics.CompositingQuality = CompositingQuality.HighQuality;
                graphics.InterpolationMode = interpolationMode;
                graphics.SmoothingMode = SmoothingMode.None;
                graphics.PixelOffsetMode = PixelOffsetMode.HighQuality;

                using (var wrapMode = new ImageAttributes())
                {
                    wrapMode.SetWrapMode(WrapMode.Clamp);
                    graphics.DrawImage(image, destRect, 0, 0, image.Width, image.Height, GraphicsUnit.Pixel, wrapMode);
                }
            }

            return destImage;
        }

        public static Bitmap SetAlpha(Bitmap bmpIn, int alpha)
        {
            Bitmap bmpOut = new Bitmap(bmpIn.Width, bmpIn.Height);
            float a = alpha / 255f;
            Rectangle r = new Rectangle(0, 0, bmpIn.Width, bmpIn.Height);

            float[][] matrixItems = {
        new float[] {1, 0, 0, 0, 0},
        new float[] {0, 1, 0, 0, 0},
        new float[] {0, 0, 1, 0, 0},
        new float[] {0, 0, 0, a, 0},
        new float[] {0, 0, 0, 0, 1}};

            ColorMatrix colorMatrix = new ColorMatrix(matrixItems);

            ImageAttributes imageAtt = new ImageAttributes();
            imageAtt.SetColorMatrix(colorMatrix, ColorMatrixFlag.Default, ColorAdjustType.Bitmap);

            using (Graphics g = Graphics.FromImage(bmpOut))
                g.DrawImage(bmpIn, r, r.X, r.Y, r.Width, r.Height, GraphicsUnit.Pixel, imageAtt);

            return bmpOut;
        }

        public static Bitmap ChangeDimensions(Bitmap bmpIn, int left, int right, int bottom, int top)
        {
            Rectangle cropArea = new Rectangle(left, top, right - left, bottom - top);

            return bmpIn.Clone(cropArea, bmpIn.PixelFormat);
        }

        public static Bitmap ChangeIntensity(Bitmap bmpIn, int min, int max)
        {
            Bitmap bmpOut = new Bitmap(bmpIn);
            bmpIn.Dispose();

            for (int x = 0; x < bmpOut.Width; x++)
            {
                for (int y = 0; y < bmpOut.Height; y++)
                {
                    var pixel = bmpOut.GetPixel(x, y);
                    int intensity = pixel.R;
                    if (intensity < pixel.G)
                        intensity = pixel.G;
                    if (intensity < pixel.B)
                        intensity = pixel.B;

                    bool pixelIsWhite = pixel == Color.White;

                    if (!pixelIsWhite && (intensity < min || intensity > max))
                        bmpOut.SetPixel(x, y, Color.FromArgb(255, 255, 255, 255));
                }
            }

            return bmpOut;
        }
    }
}
