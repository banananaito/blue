﻿namespace ImageClustering
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pictureBox = new System.Windows.Forms.PictureBox();
            this.clusterizeButton = new System.Windows.Forms.Button();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.openImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveImageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.overlayPictureBox = new System.Windows.Forms.PictureBox();
            this.horizontalTextBox = new System.Windows.Forms.TextBox();
            this.horizontalLabel = new System.Windows.Forms.Label();
            this.verticalLabel = new System.Windows.Forms.Label();
            this.verticalTextBox = new System.Windows.Forms.TextBox();
            this.opacityTrackBar = new System.Windows.Forms.TrackBar();
            this.opacityLabel = new System.Windows.Forms.Label();
            this.clusterCountLabel = new System.Windows.Forms.Label();
            this.clusterCountTextBox = new System.Windows.Forms.TextBox();
            this.algorithmLabel = new System.Windows.Forms.Label();
            this.algorithmComboBox = new System.Windows.Forms.ComboBox();
            this.labelWidth = new System.Windows.Forms.Label();
            this.labelHeight = new System.Windows.Forms.Label();
            this.numericUpDownLeft = new System.Windows.Forms.NumericUpDown();
            this.labelLeft = new System.Windows.Forms.Label();
            this.labelRight = new System.Windows.Forms.Label();
            this.numericUpDownRight = new System.Windows.Forms.NumericUpDown();
            this.labelTop = new System.Windows.Forms.Label();
            this.numericUpDownTop = new System.Windows.Forms.NumericUpDown();
            this.labelBottom = new System.Windows.Forms.Label();
            this.numericUpDownBottom = new System.Windows.Forms.NumericUpDown();
            this.labelWidthValue = new System.Windows.Forms.Label();
            this.labelHeightValue = new System.Windows.Forms.Label();
            this.buttonReset = new System.Windows.Forms.Button();
            this.labelMax = new System.Windows.Forms.Label();
            this.numericUpDownMax = new System.Windows.Forms.NumericUpDown();
            this.labelMin = new System.Windows.Forms.Label();
            this.numericUpDownMin = new System.Windows.Forms.NumericUpDown();
            this.groupBoxIntensity = new System.Windows.Forms.GroupBox();
            this.groupBoxDimensions = new System.Windows.Forms.GroupBox();
            this.groupBoxClusterization = new System.Windows.Forms.GroupBox();
            this.buttonResetIntensity = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).BeginInit();
            this.menuStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overlayPictureBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLeft)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRight)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTop)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBottom)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMax)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).BeginInit();
            this.groupBoxIntensity.SuspendLayout();
            this.groupBoxDimensions.SuspendLayout();
            this.groupBoxClusterization.SuspendLayout();
            this.SuspendLayout();
            // 
            // pictureBox
            // 
            this.pictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox.BackColor = System.Drawing.SystemColors.ControlLight;
            this.pictureBox.Location = new System.Drawing.Point(285, 37);
            this.pictureBox.Name = "pictureBox";
            this.pictureBox.Size = new System.Drawing.Size(623, 426);
            this.pictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox.TabIndex = 0;
            this.pictureBox.TabStop = false;
            // 
            // clusterizeButton
            // 
            this.clusterizeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clusterizeButton.Location = new System.Drawing.Point(135, 151);
            this.clusterizeButton.Name = "clusterizeButton";
            this.clusterizeButton.Size = new System.Drawing.Size(120, 23);
            this.clusterizeButton.TabIndex = 1;
            this.clusterizeButton.Text = "Clusterize";
            this.clusterizeButton.UseVisualStyleBackColor = true;
            this.clusterizeButton.Click += new System.EventHandler(this.ClusterizeButton_Click);
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(920, 24);
            this.menuStrip.TabIndex = 2;
            this.menuStrip.Text = "menuStrip";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openImageToolStripMenuItem,
            this.saveImageToolStripMenuItem,
            this.toolStripSeparator1,
            this.aboutToolStripMenuItem,
            this.toolStripSeparator2,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "File";
            // 
            // openImageToolStripMenuItem
            // 
            this.openImageToolStripMenuItem.Name = "openImageToolStripMenuItem";
            this.openImageToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.openImageToolStripMenuItem.Text = "Open Image";
            this.openImageToolStripMenuItem.Click += new System.EventHandler(this.AddImageToolStripMenuItem_Click);
            // 
            // saveImageToolStripMenuItem
            // 
            this.saveImageToolStripMenuItem.Name = "saveImageToolStripMenuItem";
            this.saveImageToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.saveImageToolStripMenuItem.Text = "Save Image";
            this.saveImageToolStripMenuItem.Click += new System.EventHandler(this.SaveImageToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(136, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.aboutToolStripMenuItem.Text = "About";
            this.aboutToolStripMenuItem.Click += new System.EventHandler(this.AboutToolStripMenuItem_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(136, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(139, 22);
            this.exitToolStripMenuItem.Text = "Exit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolStripMenuItem_Click);
            // 
            // overlayPictureBox
            // 
            this.overlayPictureBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.overlayPictureBox.BackColor = System.Drawing.Color.Transparent;
            this.overlayPictureBox.Location = new System.Drawing.Point(285, 37);
            this.overlayPictureBox.Name = "overlayPictureBox";
            this.overlayPictureBox.Size = new System.Drawing.Size(623, 426);
            this.overlayPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.overlayPictureBox.TabIndex = 3;
            this.overlayPictureBox.TabStop = false;
            // 
            // horizontalTextBox
            // 
            this.horizontalTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.horizontalTextBox.Location = new System.Drawing.Point(70, 85);
            this.horizontalTextBox.Name = "horizontalTextBox";
            this.horizontalTextBox.Size = new System.Drawing.Size(59, 20);
            this.horizontalTextBox.TabIndex = 4;
            this.horizontalTextBox.Text = "10";
            // 
            // horizontalLabel
            // 
            this.horizontalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.horizontalLabel.AutoSize = true;
            this.horizontalLabel.Location = new System.Drawing.Point(10, 88);
            this.horizontalLabel.Name = "horizontalLabel";
            this.horizontalLabel.Size = new System.Drawing.Size(54, 13);
            this.horizontalLabel.TabIndex = 5;
            this.horizontalLabel.Text = "Horizontal";
            // 
            // verticalLabel
            // 
            this.verticalLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verticalLabel.AutoSize = true;
            this.verticalLabel.Location = new System.Drawing.Point(143, 88);
            this.verticalLabel.Name = "verticalLabel";
            this.verticalLabel.Size = new System.Drawing.Size(42, 13);
            this.verticalLabel.TabIndex = 7;
            this.verticalLabel.Text = "Vertical";
            // 
            // verticalTextBox
            // 
            this.verticalTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.verticalTextBox.Location = new System.Drawing.Point(191, 85);
            this.verticalTextBox.Name = "verticalTextBox";
            this.verticalTextBox.Size = new System.Drawing.Size(63, 20);
            this.verticalTextBox.TabIndex = 6;
            this.verticalTextBox.Text = "10";
            // 
            // opacityTrackBar
            // 
            this.opacityTrackBar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.opacityTrackBar.LargeChange = 32;
            this.opacityTrackBar.Location = new System.Drawing.Point(9, 40);
            this.opacityTrackBar.Maximum = 255;
            this.opacityTrackBar.Name = "opacityTrackBar";
            this.opacityTrackBar.Size = new System.Drawing.Size(246, 45);
            this.opacityTrackBar.TabIndex = 8;
            this.opacityTrackBar.TickFrequency = 32;
            this.opacityTrackBar.Value = 255;
            this.opacityTrackBar.Scroll += new System.EventHandler(this.OpacityTrackBar_Scroll);
            // 
            // opacityLabel
            // 
            this.opacityLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.opacityLabel.AutoSize = true;
            this.opacityLabel.Location = new System.Drawing.Point(10, 24);
            this.opacityLabel.Name = "opacityLabel";
            this.opacityLabel.Size = new System.Drawing.Size(43, 13);
            this.opacityLabel.TabIndex = 9;
            this.opacityLabel.Text = "Opacity";
            // 
            // clusterCountLabel
            // 
            this.clusterCountLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clusterCountLabel.AutoSize = true;
            this.clusterCountLabel.Location = new System.Drawing.Point(141, 122);
            this.clusterCountLabel.Name = "clusterCountLabel";
            this.clusterCountLabel.Size = new System.Drawing.Size(44, 13);
            this.clusterCountLabel.TabIndex = 11;
            this.clusterCountLabel.Text = "Clusters";
            // 
            // clusterCountTextBox
            // 
            this.clusterCountTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.clusterCountTextBox.Location = new System.Drawing.Point(191, 119);
            this.clusterCountTextBox.Name = "clusterCountTextBox";
            this.clusterCountTextBox.Size = new System.Drawing.Size(63, 20);
            this.clusterCountTextBox.TabIndex = 10;
            this.clusterCountTextBox.Text = "5";
            // 
            // algorithmLabel
            // 
            this.algorithmLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.algorithmLabel.AutoSize = true;
            this.algorithmLabel.Location = new System.Drawing.Point(10, 122);
            this.algorithmLabel.Name = "algorithmLabel";
            this.algorithmLabel.Size = new System.Drawing.Size(50, 13);
            this.algorithmLabel.TabIndex = 12;
            this.algorithmLabel.Text = "Algorithm";
            // 
            // algorithmComboBox
            // 
            this.algorithmComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.algorithmComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.algorithmComboBox.FormattingEnabled = true;
            this.algorithmComboBox.Items.AddRange(new object[] {
            "K-means",
            "Threshold"});
            this.algorithmComboBox.Location = new System.Drawing.Point(9, 152);
            this.algorithmComboBox.Name = "algorithmComboBox";
            this.algorithmComboBox.Size = new System.Drawing.Size(120, 21);
            this.algorithmComboBox.TabIndex = 13;
            // 
            // labelWidth
            // 
            this.labelWidth.AutoSize = true;
            this.labelWidth.Location = new System.Drawing.Point(4, 22);
            this.labelWidth.Name = "labelWidth";
            this.labelWidth.Size = new System.Drawing.Size(35, 13);
            this.labelWidth.TabIndex = 14;
            this.labelWidth.Text = "Width";
            // 
            // labelHeight
            // 
            this.labelHeight.AutoSize = true;
            this.labelHeight.Location = new System.Drawing.Point(144, 22);
            this.labelHeight.Name = "labelHeight";
            this.labelHeight.Size = new System.Drawing.Size(38, 13);
            this.labelHeight.TabIndex = 15;
            this.labelHeight.Text = "Height";
            // 
            // numericUpDownLeft
            // 
            this.numericUpDownLeft.Location = new System.Drawing.Point(66, 48);
            this.numericUpDownLeft.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownLeft.Name = "numericUpDownLeft";
            this.numericUpDownLeft.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownLeft.TabIndex = 17;
            this.numericUpDownLeft.ValueChanged += new System.EventHandler(this.numericUpDownLeft_ValueChanged);
            // 
            // labelLeft
            // 
            this.labelLeft.AutoSize = true;
            this.labelLeft.Location = new System.Drawing.Point(4, 50);
            this.labelLeft.Name = "labelLeft";
            this.labelLeft.Size = new System.Drawing.Size(25, 13);
            this.labelLeft.TabIndex = 18;
            this.labelLeft.Text = "Left";
            // 
            // labelRight
            // 
            this.labelRight.AutoSize = true;
            this.labelRight.Location = new System.Drawing.Point(144, 50);
            this.labelRight.Name = "labelRight";
            this.labelRight.Size = new System.Drawing.Size(32, 13);
            this.labelRight.TabIndex = 20;
            this.labelRight.Text = "Right";
            // 
            // numericUpDownRight
            // 
            this.numericUpDownRight.Location = new System.Drawing.Point(191, 48);
            this.numericUpDownRight.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownRight.Name = "numericUpDownRight";
            this.numericUpDownRight.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownRight.TabIndex = 19;
            this.numericUpDownRight.ValueChanged += new System.EventHandler(this.numericUpDownRight_ValueChanged);
            // 
            // labelTop
            // 
            this.labelTop.AutoSize = true;
            this.labelTop.Location = new System.Drawing.Point(144, 76);
            this.labelTop.Name = "labelTop";
            this.labelTop.Size = new System.Drawing.Size(26, 13);
            this.labelTop.TabIndex = 22;
            this.labelTop.Text = "Top";
            // 
            // numericUpDownTop
            // 
            this.numericUpDownTop.Location = new System.Drawing.Point(191, 74);
            this.numericUpDownTop.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownTop.Name = "numericUpDownTop";
            this.numericUpDownTop.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownTop.TabIndex = 21;
            this.numericUpDownTop.ValueChanged += new System.EventHandler(this.numericUpDownTop_ValueChanged);
            // 
            // labelBottom
            // 
            this.labelBottom.AutoSize = true;
            this.labelBottom.Location = new System.Drawing.Point(4, 76);
            this.labelBottom.Name = "labelBottom";
            this.labelBottom.Size = new System.Drawing.Size(40, 13);
            this.labelBottom.TabIndex = 24;
            this.labelBottom.Text = "Bottom";
            // 
            // numericUpDownBottom
            // 
            this.numericUpDownBottom.Location = new System.Drawing.Point(66, 74);
            this.numericUpDownBottom.Maximum = new decimal(new int[] {
            0,
            0,
            0,
            0});
            this.numericUpDownBottom.Name = "numericUpDownBottom";
            this.numericUpDownBottom.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownBottom.TabIndex = 23;
            this.numericUpDownBottom.ValueChanged += new System.EventHandler(this.numericUpDownBottom_ValueChanged);
            // 
            // labelWidthValue
            // 
            this.labelWidthValue.Location = new System.Drawing.Point(64, 22);
            this.labelWidthValue.Name = "labelWidthValue";
            this.labelWidthValue.Size = new System.Drawing.Size(46, 13);
            this.labelWidthValue.TabIndex = 25;
            this.labelWidthValue.Text = "0";
            this.labelWidthValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // labelHeightValue
            // 
            this.labelHeightValue.Location = new System.Drawing.Point(206, 22);
            this.labelHeightValue.Name = "labelHeightValue";
            this.labelHeightValue.Size = new System.Drawing.Size(45, 13);
            this.labelHeightValue.TabIndex = 26;
            this.labelHeightValue.Text = "0";
            this.labelHeightValue.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // buttonReset
            // 
            this.buttonReset.Location = new System.Drawing.Point(134, 100);
            this.buttonReset.Name = "buttonReset";
            this.buttonReset.Size = new System.Drawing.Size(120, 23);
            this.buttonReset.TabIndex = 27;
            this.buttonReset.Text = "Reset";
            this.buttonReset.UseVisualStyleBackColor = true;
            this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
            // 
            // labelMax
            // 
            this.labelMax.AutoSize = true;
            this.labelMax.Location = new System.Drawing.Point(147, 28);
            this.labelMax.Name = "labelMax";
            this.labelMax.Size = new System.Drawing.Size(27, 13);
            this.labelMax.TabIndex = 31;
            this.labelMax.Text = "Max";
            // 
            // numericUpDownMax
            // 
            this.numericUpDownMax.Location = new System.Drawing.Point(192, 26);
            this.numericUpDownMax.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownMax.Name = "numericUpDownMax";
            this.numericUpDownMax.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownMax.TabIndex = 30;
            this.numericUpDownMax.Value = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownMax.ValueChanged += new System.EventHandler(this.numericUpDownMax_ValueChanged);
            // 
            // labelMin
            // 
            this.labelMin.AutoSize = true;
            this.labelMin.Location = new System.Drawing.Point(6, 28);
            this.labelMin.Name = "labelMin";
            this.labelMin.Size = new System.Drawing.Size(24, 13);
            this.labelMin.TabIndex = 29;
            this.labelMin.Text = "Min";
            // 
            // numericUpDownMin
            // 
            this.numericUpDownMin.Location = new System.Drawing.Point(67, 26);
            this.numericUpDownMin.Maximum = new decimal(new int[] {
            255,
            0,
            0,
            0});
            this.numericUpDownMin.Name = "numericUpDownMin";
            this.numericUpDownMin.Size = new System.Drawing.Size(63, 20);
            this.numericUpDownMin.TabIndex = 28;
            this.numericUpDownMin.ValueChanged += new System.EventHandler(this.numericUpDownMin_ValueChanged);
            // 
            // groupBoxIntensity
            // 
            this.groupBoxIntensity.Controls.Add(this.buttonResetIntensity);
            this.groupBoxIntensity.Controls.Add(this.labelMin);
            this.groupBoxIntensity.Controls.Add(this.labelMax);
            this.groupBoxIntensity.Controls.Add(this.numericUpDownMin);
            this.groupBoxIntensity.Controls.Add(this.numericUpDownMax);
            this.groupBoxIntensity.Location = new System.Drawing.Point(12, 167);
            this.groupBoxIntensity.Name = "groupBoxIntensity";
            this.groupBoxIntensity.Size = new System.Drawing.Size(262, 85);
            this.groupBoxIntensity.TabIndex = 32;
            this.groupBoxIntensity.TabStop = false;
            this.groupBoxIntensity.Text = "Intensity";
            // 
            // groupBoxDimensions
            // 
            this.groupBoxDimensions.Controls.Add(this.labelRight);
            this.groupBoxDimensions.Controls.Add(this.labelWidth);
            this.groupBoxDimensions.Controls.Add(this.buttonReset);
            this.groupBoxDimensions.Controls.Add(this.labelHeight);
            this.groupBoxDimensions.Controls.Add(this.labelHeightValue);
            this.groupBoxDimensions.Controls.Add(this.numericUpDownLeft);
            this.groupBoxDimensions.Controls.Add(this.labelWidthValue);
            this.groupBoxDimensions.Controls.Add(this.labelLeft);
            this.groupBoxDimensions.Controls.Add(this.labelBottom);
            this.groupBoxDimensions.Controls.Add(this.numericUpDownRight);
            this.groupBoxDimensions.Controls.Add(this.numericUpDownBottom);
            this.groupBoxDimensions.Controls.Add(this.numericUpDownTop);
            this.groupBoxDimensions.Controls.Add(this.labelTop);
            this.groupBoxDimensions.Location = new System.Drawing.Point(12, 27);
            this.groupBoxDimensions.Name = "groupBoxDimensions";
            this.groupBoxDimensions.Size = new System.Drawing.Size(262, 134);
            this.groupBoxDimensions.TabIndex = 33;
            this.groupBoxDimensions.TabStop = false;
            this.groupBoxDimensions.Text = "Dimensions";
            // 
            // groupBoxClusterization
            // 
            this.groupBoxClusterization.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBoxClusterization.Controls.Add(this.opacityLabel);
            this.groupBoxClusterization.Controls.Add(this.clusterizeButton);
            this.groupBoxClusterization.Controls.Add(this.horizontalTextBox);
            this.groupBoxClusterization.Controls.Add(this.algorithmComboBox);
            this.groupBoxClusterization.Controls.Add(this.horizontalLabel);
            this.groupBoxClusterization.Controls.Add(this.algorithmLabel);
            this.groupBoxClusterization.Controls.Add(this.verticalTextBox);
            this.groupBoxClusterization.Controls.Add(this.clusterCountLabel);
            this.groupBoxClusterization.Controls.Add(this.verticalLabel);
            this.groupBoxClusterization.Controls.Add(this.clusterCountTextBox);
            this.groupBoxClusterization.Controls.Add(this.opacityTrackBar);
            this.groupBoxClusterization.Location = new System.Drawing.Point(12, 278);
            this.groupBoxClusterization.Name = "groupBoxClusterization";
            this.groupBoxClusterization.Size = new System.Drawing.Size(262, 185);
            this.groupBoxClusterization.TabIndex = 34;
            this.groupBoxClusterization.TabStop = false;
            this.groupBoxClusterization.Text = "Clusterization";
            // 
            // buttonResetIntensity
            // 
            this.buttonResetIntensity.Location = new System.Drawing.Point(134, 52);
            this.buttonResetIntensity.Name = "buttonResetIntensity";
            this.buttonResetIntensity.Size = new System.Drawing.Size(120, 23);
            this.buttonResetIntensity.TabIndex = 28;
            this.buttonResetIntensity.Text = "Reset";
            this.buttonResetIntensity.UseVisualStyleBackColor = true;
            this.buttonResetIntensity.Click += new System.EventHandler(this.buttonResetIntensity_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(920, 476);
            this.Controls.Add(this.groupBoxClusterization);
            this.Controls.Add(this.groupBoxDimensions);
            this.Controls.Add(this.groupBoxIntensity);
            this.Controls.Add(this.overlayPictureBox);
            this.Controls.Add(this.pictureBox);
            this.Controls.Add(this.menuStrip);
            this.Name = "Form1";
            this.Text = "Image Clustering";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox)).EndInit();
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.overlayPictureBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.opacityTrackBar)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownLeft)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownRight)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownTop)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownBottom)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMax)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDownMin)).EndInit();
            this.groupBoxIntensity.ResumeLayout(false);
            this.groupBoxIntensity.PerformLayout();
            this.groupBoxDimensions.ResumeLayout(false);
            this.groupBoxDimensions.PerformLayout();
            this.groupBoxClusterization.ResumeLayout(false);
            this.groupBoxClusterization.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pictureBox;
        private System.Windows.Forms.Button clusterizeButton;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openImageToolStripMenuItem;
        private System.Windows.Forms.PictureBox overlayPictureBox;
        private System.Windows.Forms.TextBox horizontalTextBox;
        private System.Windows.Forms.Label horizontalLabel;
        private System.Windows.Forms.Label verticalLabel;
        private System.Windows.Forms.TextBox verticalTextBox;
        private System.Windows.Forms.TrackBar opacityTrackBar;
        private System.Windows.Forms.Label opacityLabel;
        private System.Windows.Forms.ToolStripMenuItem saveImageToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.Label clusterCountLabel;
        private System.Windows.Forms.TextBox clusterCountTextBox;
        private System.Windows.Forms.Label algorithmLabel;
        private System.Windows.Forms.ComboBox algorithmComboBox;
        private System.Windows.Forms.Label labelWidth;
        private System.Windows.Forms.Label labelHeight;
        private System.Windows.Forms.NumericUpDown numericUpDownLeft;
        private System.Windows.Forms.Label labelLeft;
        private System.Windows.Forms.Label labelRight;
        private System.Windows.Forms.NumericUpDown numericUpDownRight;
        private System.Windows.Forms.Label labelTop;
        private System.Windows.Forms.NumericUpDown numericUpDownTop;
        private System.Windows.Forms.Label labelBottom;
        private System.Windows.Forms.NumericUpDown numericUpDownBottom;
        private System.Windows.Forms.Label labelWidthValue;
        private System.Windows.Forms.Label labelHeightValue;
        private System.Windows.Forms.Button buttonReset;
        private System.Windows.Forms.Label labelMax;
        private System.Windows.Forms.NumericUpDown numericUpDownMax;
        private System.Windows.Forms.Label labelMin;
        private System.Windows.Forms.NumericUpDown numericUpDownMin;
        private System.Windows.Forms.GroupBox groupBoxIntensity;
        private System.Windows.Forms.GroupBox groupBoxDimensions;
        private System.Windows.Forms.GroupBox groupBoxClusterization;
        private System.Windows.Forms.Button buttonResetIntensity;
    }
}

