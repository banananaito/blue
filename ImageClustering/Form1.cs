﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace ImageClustering
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private Bitmap originalImage;
        private Bitmap originalOverlayImage;
        private String imageFileName;

        private void Form1_Load(object sender, EventArgs e)
        {
            overlayPictureBox.Parent = pictureBox;
            overlayPictureBox.Location = Point.Empty;
            algorithmComboBox.SelectedIndex = 0;
        }



        private void ClusterizeButton_Click(object sender, EventArgs e)
        {
            if (pictureBox.Image != null)
            {
                try
                {
                    Cluster cluster = new Cluster();
                    int width, height, clusterCount;
                    Int32.TryParse(horizontalTextBox.Text, out width);
                    Int32.TryParse(verticalTextBox.Text, out height);
                    Int32.TryParse(clusterCountTextBox.Text, out clusterCount);

                    if (algorithmComboBox.SelectedIndex == 0)
                    {
                        overlayPictureBox.Image = cluster.ClusterizeImageKMeans((Bitmap)pictureBox.Image, width, height, clusterCount);
                    }
                    else
                    {
                        overlayPictureBox.Image = cluster.ClusterizeImage((Bitmap)pictureBox.Image, width, height, clusterCount);
                    }

                    originalOverlayImage = (Bitmap)overlayPictureBox.Image.Clone();
                    // to show same alpha
                    OpacityTrackBar_Scroll(null, null);
            }
                catch (Exception)
            {
                MessageBox.Show("Please check your cluster values.", "Error");
            }
        }
        }

        private void OpacityTrackBar_Scroll(object sender, EventArgs e)
        {
            if (overlayPictureBox.Image != null)
            {
                overlayPictureBox.BackColor = Color.Transparent;
                overlayPictureBox.Image.Dispose();
                overlayPictureBox.Image = Cluster.SetAlpha(originalOverlayImage, opacityTrackBar.Value);
            }
        }

        private void AddImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenFileDialog openDialog = new OpenFileDialog();
            openDialog.Filter = "Image Files(*.jpg; *.jpeg; *.gif; *.bmp)|*.jpg; *.jpeg; *.gif; *.bmp";
            if (openDialog.ShowDialog() == DialogResult.OK)
            {
                originalImage = null;

                imageFileName = openDialog.FileName;
                pictureBox.Image = new Bitmap(imageFileName);
                overlayPictureBox.Image = null;

                labelWidthValue.Text = pictureBox.Image.Width.ToString();
                labelHeightValue.Text = pictureBox.Image.Height.ToString();

                numericUpDownLeft.Maximum = pictureBox.Image.Width;
                numericUpDownRight.Maximum = pictureBox.Image.Width;
                numericUpDownTop.Maximum = pictureBox.Image.Height;
                numericUpDownBottom.Maximum = pictureBox.Image.Height;

                numericUpDownLeft.Value = 0;
                numericUpDownRight.Value = numericUpDownRight.Maximum;
                numericUpDownTop.Value = 0;
                numericUpDownBottom.Value = numericUpDownBottom.Maximum;

                numericUpDownMin.Value = 0;
                numericUpDownMax.Value = 255;

                originalImage = new Bitmap(imageFileName);
            }
        }

        private void SaveImageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (originalOverlayImage != null)
            {
                var saveDialog = new SaveFileDialog();
                saveDialog.FileName = imageFileName + "_result";
                saveDialog.DefaultExt = "png";
                saveDialog.Filter = "PNG image (*.png)|*.png";

                if (saveDialog.ShowDialog() == DialogResult.OK)
                {
                    originalOverlayImage.Save(saveDialog.FileName, System.Drawing.Imaging.ImageFormat.Png);
                }
            }
        }

        private void AboutToolStripMenuItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Author: Ivan Mykulanynets\nVersion: 0.4.0", "Image Clustering");
        }

        private void ExitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ChangeDimensions()
        {
            try
            {
                pictureBox.Image.Dispose();
                pictureBox.Image = Cluster.ChangeDimensions(originalImage, (int)numericUpDownLeft.Value, (int)numericUpDownRight.Value, (int)numericUpDownBottom.Value, (int)numericUpDownTop.Value);
                if (overlayPictureBox.Image != null)
                {
                    overlayPictureBox.Image = null;
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Please check your dimension values.", "Error");
            }
            
        }

        private void numericUpDownLeft_ValueChanged(object sender, EventArgs e)
        {
            ProcessImage();
        }

        private void numericUpDownRight_ValueChanged(object sender, EventArgs e)
        {
            ProcessImage();
        }

        private void numericUpDownBottom_ValueChanged(object sender, EventArgs e)
        {
            ProcessImage();
        }

        private void numericUpDownTop_ValueChanged(object sender, EventArgs e)
        {
            ProcessImage();
        }

        private void buttonReset_Click(object sender, EventArgs e)
        {
            if (originalImage != null)
            {
                numericUpDownLeft.Value = 0;
                numericUpDownRight.Value = originalImage.Width;
                numericUpDownTop.Value = 0;
                numericUpDownBottom.Value = originalImage.Height;
            }
        }

        private void ChangeIntensity()
        {
            if (pictureBox.Image != null && (numericUpDownMin.Value != 0 || numericUpDownMax.Value != 255))
            { 
                pictureBox.Image = Cluster.ChangeIntensity(new Bitmap(pictureBox.Image), (int)numericUpDownMin.Value, (int)numericUpDownMax.Value);
                if (overlayPictureBox.Image != null)
                {
                    overlayPictureBox.Image = null;
                }
            }
        }

        private void numericUpDownMin_ValueChanged(object sender, EventArgs e)
        {
            ProcessImage();
        }

        private void numericUpDownMax_ValueChanged(object sender, EventArgs e)
        {
            ProcessImage();
        }

        private void buttonResetIntensity_Click(object sender, EventArgs e)
        {
            numericUpDownMin.Value = 0;
            numericUpDownMax.Value = 255;
        }

        private void ProcessImage()
        {
            if (originalImage != null)
            {
                ChangeDimensions();
                ChangeIntensity();
            }
        }
    }
}
